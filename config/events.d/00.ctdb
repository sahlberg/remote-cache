#!/bin/sh
############################
# main event script for ctdb
#
# This script is called with one of the following sets of arguments
#     startup    : called when ctdb starts
#     shutdown   : called when ctdb shuts down
#     takeip     : called when an IP address is taken over
#     releaseip  : called when an IP address is released
#     recovered  : called when ctdb has finished a recovery event

. $CTDB_BASE/functions
loadconfig ctdb

# ensure we have /bin and /usr/bin in the path
PATH=/bin:/usr/bin:$PATH

cmd="$1"
shift

# set default samba cleanup period - in minutes
[ -z "$CTDB_VACUUM_PERIOD" ] && {
    CTDB_VACUUM_PERIOD=5
}

###########################
# periodic vacuum function
periodic_vacuum() {
    # this cleans up dead records and repacks the databases
    ( time ctdb vacuum 200000 -T 30; time ctdb repack -T 30 ) > $CTDB_BASE/state/vacuum.log 2>&1 &
}

case $cmd in 
     startup)
        # make sure we have a blank state directory for the scripts to work with
	/bin/rm -rf $CTDB_BASE/state
	/bin/mkdir -p $CTDB_BASE/state

	# set any tunables from the config file
	set | grep ^CTDB_SET_ | cut -d_ -f3- | 
	while read v; do
	    varname=`echo $v | cut -d= -f1`
	    value=`echo $v | cut -d= -f2`
	    ctdb setvar $varname $value || exit 1
	    echo "Set $varname to $value"
	done || exit 1
	;;

    monitor)
	# Create a dummy file to track when we need to do periodic cleanup
	# of samba databases
	[ -f $CTDB_BASE/state/periodic_vacuum ] || {
		touch $CTDB_BASE/state/periodic_vacuum
	}
	[ `/usr/bin/find $CTDB_BASE/state/periodic_vacuum -mmin +$CTDB_VACUUM_PERIOD | wc -l` -eq 1 ] && {
		# vacuum the databases
		touch $CTDB_BASE/state/periodic_vacuum
	    	periodic_vacuum
	}

	# monitor that we are not running out of memory
	[ -z "$CTDB_MONITOR_FREE_MEMORY" ] || {
		FREE_MEM=`free -m | grep "buffers/cache" | while read A B C D ;do /bin/echo -n $D ; done`
		[ `expr "$FREE_MEM" "<" "$CTDB_MONITOR_FREE_MEMORY"` != "0" ] && {
			echo "OOM. Free:$FREE_MEM while CTDB treshold is $CTDB_MONITOR_FREE_MEMORY"
			ctdb disable
			sleep 3
			ctdb shutdown
		}
	}
esac

# all OK
exit 0
