%define initdir %{_sysconfdir}/init.d

Summary: Remote Read-Only NFS cache
Vendor: Ronnie Sahlberg
Packager: Ronnie Sahlberg <ronniesahlberg@gmail.com>
Name: remote-cache
Version: 3.6
Release: 0
Epoch: 0
License: GNU GPL version 3
Group: System Environment/Daemons
URL: http://none.yet

Source: remote-cache-%{version}.tar.gz

Prereq: /sbin/chkconfig /bin/mktemp /usr/bin/killall
Prereq: fileutils sed /etc/init.d

Provides: remote-cache = %{version}

Prefix: /usr
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
remote cache is a tool to remotely cache and reexport a networked filesystem


#######################################################################

%prep
%setup -q
# setup the init script and sysconfig file
%setup -T -D -n remote-cache-%{version} -q

%build

CC="gcc"

## always run autogen.sh
./autogen.sh

CFLAGS="$RPM_OPT_FLAGS $EXTRA -O0 -D_GNU_SOURCE -DREMOTE_CACHE_VERS=\"%{version}-%{release}\"" ./configure \
	--prefix=%{_prefix} \
	--sysconfdir=%{_sysconfdir} \
	--mandir=%{_mandir} \
	--localstatedir="/var"

make showflags
make   

%install
# Clean up in case there is trash left from a previous build
rm -rf $RPM_BUILD_ROOT

# Create the target build directory hierarchy
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/init.d
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1

make DESTDIR=$RPM_BUILD_ROOT install

# Remove "*.old" files
find $RPM_BUILD_ROOT -name "*.old" -exec rm -f {} \;

%clean
rm -rf $RPM_BUILD_ROOT

%post

%preun

%postun


#######################################################################
## Files section                                                     ##
#######################################################################

%files
%defattr(-,root,root)

%{_bindir}/remote-cache
%{_mandir}/man1/remote-cache.1.gz

%changelog
* Fri Jul 24 2009 : Version 3.6
 - Add a cleanup daemon/tool from martins
* Wed Jul 15 2009 : Version 3.5
 - Internal cleanup of the code and removal of all uses of switching uid.
   Uids are instead checked explicitely in the code and i/o is always performed as the root user.
* Tue Apr 7 2009 : Version 3.4
 - add pregenerated manpages
 - when installing rpm replace any manpages from previous versions with the new manpage.
 - check that the umount actually worked for --unexport and only proceed to terminate the migration process if successful
 - use mutexes to protect the acccess to the tdb databases from the pthreads
 - keep an array of the most recently migrated files to avoid spamming the migration queue with the same file over and over
* Mon Mar 30 2009 : Version 3.3
 - Create a --unexport option that can be used to cleanly shutdown the multiprocess model for an export.
* Fri Mar 20 2009 : Version 3.2
 - remove all remaning talloc calls
 - remove additional instances where we switch between root and the real user
* Wed Mar 10 2009 : Version 3.1
 - Remove calls to talloc in performance critical paths
* Mon Mar 9 2009 : Version 3.0
 - split remote-cache into a two process model where a dedicated process
   manages all replications.
* Fri Feb 27 2009 : Version 2.9.0
 - Updates and many many performance tweaks
* Tue Nov 25 2008 : Version 2.5.0
 - plug some memory leaks
* Sun Nov 23 2008 : Version 2.4.0
 - Improved speed when writing files
* Sat Nov 15 2008 : Version 2.3.0
 - plug a filedescriptor leak for the read/write mode
* Wed Nov 12 2008 : Version 2.2.0
 - Use -o big_writes for significantly better write performance
* Sun Nov 9 2008 : Version 2.1.0
 - bugfixes for read-write support
* Wed Jul 23 2008 : Version 2.0.0
 - Add experimental read/write support 
 - Add a --file-min-mtime-age. Dont cache files that have been modified
   more recently than this.
* Tue Jul 1 2008 : Version 1.0.10
 - Remove some utility functions we dont need.
 - Add -file-check-parent-mtime flag to activate the optimization
   for archive systems (files are never modified post creation)
* Tue Jun 24 2008 : Version 1.0.9
 - Permissions updates
* Wed Jun 18 2008 : Version 1.0.8
 - emulate the access() call
* Wed Jun 18 2008 : Version 1.0.7
 - Add a new option --file-check-parent-mtime
* Tue Jun 17 2008 : Version 1.0.6
 - Clear out the list of extra groups when we change to the real user
* Mon Jun 16 2008 : Version 1.0.5
 - Update the uid/gid and permission checks when reading directories
* Fri Jun 13 2008 : Version 1.0.4
 - Fix a bug while changinf uid/gid to become the real user.
* Tue Jun 10 2008 : Version 1.0.3
 - Add paramters --max-file-size, --file-min-blocks, --file-min-inodes,
   --dir-min-blocks and --dir-min-inodes that control when we should
   stop adding items to the local cache.
 - When emulating READDIR() make sure that we only try to cache
   the directory entries if we have enough space available in the cache
   filesystem.  This could lead to a hang.
 - Add check that --export, --remote and --cache exist and that they
   are directories.
 - Add new parameter --max-dir-cache which controls the maximum amount of time
   to cache a directory and its directory entries.
* Wed May 27 2008 : Version 1.0.2
 - Add a manpage
* Tue May 27 2008 : Version 1.0.1
 - Use popt to parse the command arguments to use.
 - Log all messages to /var/log/log.remote-cache
 - Add --debug debuglevels to control what will be logged.
