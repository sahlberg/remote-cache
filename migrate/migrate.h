/*
   fuse module to perform the vfs part of read-write remote caching for NFS
  
   Copyright Ronnie Sahlberg 2009
  
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#define discard_const(ptr) ((void *)((intptr_t)(ptr)))

extern struct tdb_context *migrate_db;
extern const char *migrate_db_path;
extern const char *cache;
extern const char *remote;
extern const char *export;

extern int this_log_level;
enum deb_lev { 
	DEBUG_EMERG   = -3,
	DEBUG_ALERT   = -2, 
	DEBUG_CRIT    = -1,
	DEBUG_ERR     =  0,
	DEBUG_WARNING =  1,
	DEBUG_NOTICE  =  2,	
	DEBUG_INFO    =  3,
	DEBUG_DEBUG   =  4,
};
#define DEBUG(lvl, x) \
	do {\
		if ((lvl) <= debug_level) {\
			this_log_level = (lvl);\
			if (!debug_initialized) {\
 				dup2(log_fd, 2);\
				debug_initialized=1;\
			}\
			do_debug x;\
	}} while (0)

extern enum deb_lev debug_level;
extern int debug_initialized;
extern int log_fd;


int start_migration_daemon(void);
int open_migrate_db(void);
pid_t fetch_migd_pid(void);

const char *get_migrate_db_path(const char *export);

#define RCVARDIR "/var/lib/remote-cache"

/* entries for files that are requested to be migrated are stored in a circular
   list.
*/
#define MIGRATE_HEADER_KEY	"migrate_header_key"

#define MIGRATE_MAX 1000

struct migrate_header {
	uint32_t first;
	uint32_t last;
	uint32_t max;
};

struct migrate_entry {
	time_t ts;
	uint32_t pathlen; 
	char path[1];
};
