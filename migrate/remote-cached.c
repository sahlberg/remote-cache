/*
   Daemon to recall/cache files requested by remote-cache
  
   Copyright Ronnie Sahlberg 2009
  
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../lib/tdb/include/tdb.h"
#include "../lib/tdb/common/tdb_private.h"
#include "../lib/talloc/talloc.h"
#include "../lib/events/events.h"
#include "../lib/util/debug.h"
#include "migrate.h"


int migrate_files(void)
{
	TALLOC_CTX *mem_ctx = NULL;
	TDB_DATA hdrkey, pathkey, data;
	struct migrate_header *mh;
	struct migrate_entry *me;
	char *old_path;
	char *new_path;
	char *path;
	char *migrate_cmd;
	int ret = 0;
	struct stat old_st;
	struct stat new_st;
	struct flock lock;
	int cache_fd = -1;

	/* Open the migrate db database */
	open_migrate_db();

	mem_ctx = talloc_new(NULL);

	/* get the header */
	hdrkey.dptr  = (uint8_t *)discard_const(MIGRATE_HEADER_KEY);
	hdrkey.dsize = strlen(MIGRATE_HEADER_KEY);

	ret = tdb_lockall(migrate_db);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to lock migration db in migrate daemon\n"));
		ret = -1;
		goto finished;
	}

	data = tdb_fetch(migrate_db, hdrkey);
	if (data.dptr == NULL) {
		DEBUG(DEBUG_INFO,(__location__ " No header structure found.\n"));
		ret = -1;
		goto finished;
	}

	mh = talloc_memdup(mem_ctx, data.dptr, data.dsize);
	free(data.dptr);
	if (mh == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to allocate copy mh structure in migrate daemon.\n"));
		ret = -1;
		goto finished;
	}

	if (mh->last == mh->first) {
		DEBUG(DEBUG_ERR,(__location__ " No file to migrate in migrate daemon. First:%d last:%d\n", mh->first, mh->last));
		ret = -1;
		goto finished;
	}

	DEBUG(DEBUG_DEBUG,("Migrate header first:%u last:%u\n", mh->first, mh->last));

	pathkey.dptr  = (uint8_t *)&mh->last;
	pathkey.dsize = sizeof(mh->last);
	data = tdb_fetch(migrate_db, pathkey);
	if (data.dptr == NULL) {
		DEBUG(DEBUG_INFO,(__location__ " No path structure found for key:%u.\n", mh->last));
		ret = -1;
		goto finished;
	}

	me = talloc_memdup(mem_ctx, data.dptr, data.dsize);
	free(data.dptr);
	if (me == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to allocate copy me structure in migrate daemon.\n"));
		ret = -1;
		goto finished;
	}

	if (data.dsize != offsetof(struct migrate_entry, path) + me->pathlen) {
	  DEBUG(DEBUG_ERR,("Wrong size of me structure. Expected %d bytes but found %d bytes.\n", (int)(offsetof(struct migrate_entry, path) + me->pathlen), (int)data.dsize));
		ret = -1;
		goto finished;
	}

	if (mh->last == 0) {
		mh->last = mh->max - 1;
	} else {
		mh->last--;
	}

	data.dptr  = (uint8_t *)mh;
	data.dsize = sizeof(*mh);
	ret = tdb_store(migrate_db, hdrkey, data, TDB_REPLACE);
	if (ret != 0 ){
		DEBUG(DEBUG_ERR,(__location__ " Failed to store migration header in migrate daemon\n"));
		goto finished;
	}


	/* we no longer need a lock on the database */
	tdb_unlockall(migrate_db);

	path = talloc_size(mem_ctx, me->pathlen + 1);
	if (path == NULL) {
		DEBUG(DEBUG_ERR,("Failed to allocate path of size:%u\n", me->pathlen+1));
		ret = -1;
		goto finished;
	}
	memcpy(path, me->path, me->pathlen);
	path[me->pathlen]=0;

	old_path = talloc_asprintf(mem_ctx, "%s/%s", remote, path);
	new_path = talloc_asprintf(mem_ctx, "%s/%s", cache, path);

	bzero(&old_st, sizeof(old_st));
	ret = lstat(old_path, &old_st);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to lstat(%s) old path.\n", old_path));
		ret = -1;
		goto finished;
	}
	bzero(&new_st, sizeof(new_st));
	lstat(new_path, &new_st);
	if (new_st.st_mtime == old_st.st_mtime) {
		DEBUG(DEBUG_INFO,(__location__ " File %s in cache is still valid. No need to migrate it\n", new_path));
		ret = 0;
		goto finished;
	}


	/* make sure the file exists */
	cache_fd = open(new_path, O_RDWR|O_CREAT);
	if (cache_fd == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to create cache file %s, errno:%d\n", new_path, errno));
		ret = -errno;
		goto finished;
	}
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 1;
	lock.l_pid = getpid();
	if (fcntl(cache_fd, F_SETLK, &lock) != 0) {
		/* someone else is already migrating this file */
		DEBUG(DEBUG_DEBUG, (__location__ " Failed to get lock on cache file %s.\n", new_path));
		ret = 0;
		goto finished;
	}

	migrate_cmd = talloc_asprintf(mem_ctx, "rsync -ptgo \"%s\" \"%s\"", old_path, new_path);
	DEBUG(DEBUG_DEBUG,("Running migration command %s\n", migrate_cmd));
	ret = system(migrate_cmd);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " migration %s -> %s failed\n", old_path, new_path));
		ret = -1;
		goto finished;
	}


finished:
	if (cache_fd != -1) {
		close(cache_fd);
	}
	tdb_unlockall(migrate_db);
	talloc_free(mem_ctx);
	return ret;
}

/* The main remote-cache application writes files to be read to a tdb and
   then sends us a signal to tell us we have files to process
*/
static void migd_sigusr1_handler(struct event_context *ev, struct signal_event *se, int signum, int count, void *dont_care, void *private_data)
{
	int ret = 0;

	DEBUG(DEBUG_DEBUG,("Start migration of files\n"));
	while (ret == 0) {
		ret = migrate_files();
	}
	DEBUG(DEBUG_DEBUG,("No more files to migrate, for now\n"));
}

static void migd_sigusr2_handler(struct event_context *ev, struct signal_event *se, int signum, int count, void *dont_care, void *private_data)
{
	TALLOC_CTX *mem_ctx = talloc_new(NULL);
	char *cmd;
	int ret;

	cmd = talloc_asprintf(mem_ctx, "fusermount -u %s", export);
	DEBUG(DEBUG_ERR,("Got SIGUSR2, unmounting fuse filesystem %s\n", export));
	ret = system(cmd);
	if (ret == -1) {
		DEBUG(DEBUG_ERR,("system(%s) call failed : %s\n", cmd, strerror(errno)));
		return;
	}
	ret = WEXITSTATUS(ret);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,("%s failed with return code %d. Umount failed.\n", cmd, ret));
		return;
	}

	talloc_free(mem_ctx);
	_exit(10);
}

#define MIGD_PID_KEY "migd_pid"

static int store_migd_pid(pid_t mypid)
{
	TALLOC_CTX *mem_ctx = NULL;
	TDB_DATA migdkey, data;
	int ret = 0;

	/* Open the migrate db database */
	open_migrate_db();

	mem_ctx = talloc_new(NULL);

	migdkey.dptr  = discard_const(MIGD_PID_KEY);
	migdkey.dsize = strlen(MIGD_PID_KEY);

	data.dptr  = (uint8_t *)&mypid;
	data.dsize = sizeof(mypid);

	ret = tdb_lockall(migrate_db);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to lock migration db in migrate daemon\n"));
		ret = -1;
		goto finished;
	}

	ret = tdb_store(migrate_db, migdkey, data, TDB_REPLACE);
	if (ret != 0 ){
		DEBUG(DEBUG_ERR,(__location__ " Failed to store migd pid in migrate daemon\n"));
		goto finished;
	}

finished:
	tdb_unlockall(migrate_db);
	talloc_free(mem_ctx);
	return ret;
}

pid_t fetch_migd_pid(void)
{
	TALLOC_CTX *mem_ctx = NULL;
	TDB_DATA migdkey, data;
	int ret = 0;
	pid_t migdpid = 0;

	/* Open the migrate db database */
	open_migrate_db();

	mem_ctx = talloc_new(NULL);

	migdkey.dptr  = discard_const(MIGD_PID_KEY);
	migdkey.dsize = strlen(MIGD_PID_KEY);

	ret = tdb_lockall(migrate_db);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to lock migration db in migrate daemon\n"));
		goto finished;
	}

	data = tdb_fetch(migrate_db, migdkey);
	if (data.dptr == NULL ){
		DEBUG(DEBUG_ERR,(__location__ " Failed to fetch migd pid from database\n"));
		goto finished;
	}
	if (data.dsize != sizeof(migdpid)) {
		DEBUG(DEBUG_ERR,("Wrong size for migd pid data\n"));
		goto finished;
	}
	migdpid = *((pid_t *)data.dptr);
	free(data.dptr);

finished:
	tdb_unlockall(migrate_db);
	talloc_free(mem_ctx);
	return migdpid;
}

int start_migration_daemon(void)
{
	TALLOC_CTX *migd;
	struct event_context *ev;
	struct signal_event *se;
	pid_t mypid, cpid;

	talloc_enable_null_tracking();

	mypid = getpid();
	cpid = fork();
	if (cpid == -1) {
		DEBUG(DEBUG_CRIT,("Failed to fork migration process. Aborting\n"));
		return -1;
	}
	if (cpid == 0) {
		return mypid;
	}

	migd = talloc_new(NULL);
	if (migd == NULL) {
		DEBUG(DEBUG_CRIT,("Failed to allocate migd. Aborting.\n"));
		return -1;
	}

	/* store out pid in the migration database */
	if (store_migd_pid(mypid) != 0) {
		DEBUG(DEBUG_CRIT,("Failed to store migd pid in database\n"));
		return -1;
	}


	ev = event_context_init(NULL);

	DEBUG(DEBUG_NOTICE,("Starting remote-cache migrate daemon, pid:%u\n", mypid));

	/* set up a handler to pick up sigusr1 */
	se = event_add_signal(ev, migd, SIGUSR1, 0, migd_sigusr1_handler, migd);
	if (se == NULL) {
		DEBUG(DEBUG_CRIT,("Failed to set up signal event handler for SIGUSR1. Aborting\n"));
		talloc_free(migd);
		return -1;
	}

	/* make sure we unmount the filesystem when the migration dameon exits */
	/* set up a handler to pick up sigusr2 */
	se = event_add_signal(ev, migd, SIGUSR2, 0, migd_sigusr2_handler, migd);

	event_loop_wait(ev);
	DEBUG(DEBUG_CRIT,("Returned from event loop. This can not happen\n"));

	return mypid;
}
