/*
   fuse module to perform the vfs part of read-write remote caching for NFS
  
   Copyright Ronnie Sahlberg 2008
  
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#define FUSE_USE_VERSION 26

/* we use a local lock file to limit how many files we are migrating
   across simultaneously
*/
#define FILE_MIGRATE_LIMITER     "/var/lib/remote-cache/lockfile"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/statvfs.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <dirent.h>
#include <utime.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>
#include <pthread.h>
#include "migrate.h"
#include "../lib/tdb/include/tdb.h"
#include "../lib/tdb/common/tdb_private.h"
#include "../lib/util/debug.h"
#include "popt.h"

static pthread_mutex_t groups_db_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t migrate_db_mutex = PTHREAD_MUTEX_INITIALIZER;

/* this is the directory where we cache data */
const char *cache  = NULL;

/* this is where the remote nfs server is mounted */
const char *remote = NULL;

/* this is the directory where we present the cached image and which
   we re-export through our local nfs server to the clients */
const char *export = NULL;

const char *unexport = NULL;


enum deb_lev debug_level = DEBUG_ERR;

/* the maximum time in seconds we cache a directory.
   defaults to 0 which means 'forever'
*/
static int max_dir_cache;

/* minimum number of inodes in the cache filesystem before we will stop 
   caching more directory structures.
*/
static int dir_min_inodes = 100;

/* minimum number of blocks in the cache filesystem before we will stop 
   caching more directory structures.
*/
static int dir_min_blocks = 1000;

/* minimum number of inodes in the cache filesystem before we will stop 
   caching more files.
*/
static int file_min_inodes = 100;

/* minimum number of blocks in the cache filesystem before we will stop 
   caching more files.
*/
static int file_min_blocks = 10000;

/* maximum size in MB of files to be copied to the local cache.
   0 means there is no size limit.
*/
static int file_max_size = 0;

/* if 0, we check the mtime of the parent directory instead of the
   file itself.  This speeds up things when we have an environment
   where files are created/deleted but never modified.
*/
static int file_check_parent_mtime = 0;

/* Dont migrate files that were modified less than this many
   seconds ago.
*/
static int file_min_mtime_age = 120;

/* Should we allow read/write semantics ? */
static int read_write = 0;

/* should we use direct_io or not? Direct i/o can not be reexported by kNFSd
   on linux !
*/
static int direct_io = 0;

/* should we use big_writes or not?
   on linux !
*/
static int big_writes = 0;

/* files smaller than this are always completely migrated during first read
*/
static int file_migrate_small_file = 131072;


/* pid of the migration daemon */
static pid_t migpid = -1;

struct tdb_context *migrate_db = NULL;
const char *migrate_db_path = NULL;

int debug_initialized = 0;
int log_fd = -1;

struct tdb_context *groups_db = NULL;

struct write_fragment {
	struct write_fragment *next;
	off_t offset;
	size_t size;
	char *data;
};
struct fh_struct {
	int is_nfsd;
	int num_writes;
	struct write_fragment *fragments;
};

const char *get_migrate_db_path(const char *exp)
{
	char *str, *e, *ptr;

	e = strdup(exp);
	while((ptr = index(e, '/')) != NULL) {
		*ptr = '_';
	}

	asprintf(&str, "%s/migratedb_%s.tdb", RCVARDIR, e);
	free(e);
	return str;
}

static void remote_cache_unexport(const char *unexp)
{
	pid_t migdpid;

	export = unexp;
	migdpid = fetch_migd_pid();
	if (migdpid == 0) {
		DEBUG(DEBUG_ERR,("Failed to fetch migd pid\n"));
		exit(10);
	}

	if (kill(migdpid, SIGUSR2) != 0) {
		DEBUG(DEBUG_ERR,("Failed to kill migd daemon\n"));
		exit(0);
	}
	DEBUG(DEBUG_ERR,("Unexporting %s\n", unexp));
	exit(0);
}


static int rate_limit_is_ok(void)
{
	/* only print one message every 60 seconds */
	static time_t last_time = 0;
	static time_t this_time = 0;

	this_time = time(NULL);
	
	if (this_time < last_time) {
		/* a job for s hawkins */
		last_time = this_time;
		return 1;
	}

	if (this_time > (last_time+60)) {
		last_time = this_time;
		return 1;
	}

	return 0;
}

#define MAX_GROUPS 256
struct group_list {
	time_t ts;
	int num_groups;
	gid_t groups[MAX_GROUPS];
};


#define CAN_READ    0x0001
#define CAN_WRITE   0x0002
#define CAN_EXECUTE 0x0004
static int check_permissions(struct stat *st, uid_t uid, gid_t gid, struct group_list *gl)
{
	int perms = 0;
	int i;

	/* see if any of the extra gids match the gid of the file */
	for (i=0;i<gl->num_groups;i++) {
		if (st->st_gid == gl->groups[i]) {
			gid = gl->groups[i];
			break;
		}
	}

	/* check read permissions */
	if (uid == 0) {
		perms |= CAN_READ;
	}
	if ((uid == st->st_uid) && (st->st_mode & S_IRUSR)) {
		perms |= CAN_READ;
	}
	if ((gid == st->st_gid) && (st->st_mode & S_IRGRP)) {
		perms |= CAN_READ;
	}
	if ((st->st_mode & S_IROTH)) {
		perms |= CAN_READ;
	}

	/* check write permissions */
	if (uid == 0) {
		perms |= CAN_WRITE;
	}
	if ((uid == st->st_uid) && (st->st_mode & S_IWUSR)) {
		perms |= CAN_WRITE;
	}
	if ((gid == st->st_gid) && (st->st_mode & S_IWGRP)) {
		perms |= CAN_WRITE;
	}
	if ((st->st_mode & S_IWOTH)) {
		perms |= CAN_WRITE;
	}

	/* check execute permissions */
	if (uid == 0) {
		perms |= CAN_EXECUTE;
	}
	if ((uid == st->st_uid) && (st->st_mode & S_IXUSR)) {
		perms |= CAN_EXECUTE;
	}
	if ((gid == st->st_gid) && (st->st_mode & S_IXGRP)) {
		perms |= CAN_EXECUTE;
	}
	if ((st->st_mode & S_IXOTH)) {
		perms |= CAN_EXECUTE;
	}

	return perms;
}



static int
get_group_list(uid_t uid, struct group_list *glist)
{
	int ret = -1;
	TDB_DATA data = {NULL,0};

	pthread_mutex_lock(&groups_db_mutex);
	glist->num_groups = MAX_GROUPS;

	/* make sure we have a group mapping database */
	if (groups_db == NULL) {
		DEBUG(DEBUG_INFO,("Initializing group mapping cache database\n"));
		groups_db = tdb_open("groups", 10000, TDB_INTERNAL, O_RDWR|O_CREAT, 0);
		if (groups_db == NULL) {
			DEBUG(DEBUG_ERR,("Failed to initialize group mapping database.\n"));
		}
	}

	if (groups_db) {
		TDB_DATA key, newdata;
		struct passwd *pw;

		key.dptr = (uint8_t *)&uid;
		key.dsize= sizeof(uid);

		tdb_chainlock(groups_db, key);
		data = tdb_fetch(groups_db, key);

		/* we already have a record for thus uid, check if it is still
		   valid
		*/
		if (data.dptr != NULL) {
			if (data.dsize != sizeof(*glist)) {
				DEBUG(DEBUG_ERR,("Wrong size of glist structure  was:%d expected:%d\n", (int)data.dsize, (int)sizeof(*glist)));
				free(data.dptr);
				data.dptr = NULL;
				tdb_chainunlock(groups_db, key);
				goto finished;
			}

			memcpy(glist, data.dptr, sizeof(*glist));
			free(data.dptr);
			data.dptr = NULL;

			if (glist->num_groups >= MAX_GROUPS) {
				glist->num_groups = MAX_GROUPS;
				DEBUG(DEBUG_ERR,("Invalid number of groups in glist structure (%d)\n", glist->num_groups));
			}

			if (glist->ts > time(NULL)-36000) { 
				tdb_chainunlock(groups_db, key);
				ret = 0;
				goto finished;
			}
		}				

		/* we need to re-read the list of groups from the system and
		   store it in the cache
		 */
		pw = getpwuid(uid);
		if (pw == NULL) {
			tdb_chainunlock(groups_db, key);
			goto finished;
		}

		getgrouplist(pw->pw_name, pw->pw_gid, glist->groups, &glist->num_groups);
		glist->ts = time(NULL);

		newdata.dptr  = (uint8_t *)glist;
		newdata.dsize = sizeof(*glist);
		if (tdb_store(groups_db, key, newdata, TDB_REPLACE)) {
			DEBUG(DEBUG_ERR,("Failed to write gl structure to cahce tdb\n"));
			tdb_chainunlock(groups_db, key);
			goto finished;
		}

		tdb_chainunlock(groups_db, key);
		ret = 0;
	}
finished:
	pthread_mutex_unlock(&groups_db_mutex);
	return ret;
}


int verify_permission(uid_t uid, gid_t gid, char *path, int required)
{
	int ret;
	int perms;
	struct stat st;
	struct group_list gl;

	/* read parent attributes from remote site */
	ret = get_group_list(uid, &gl);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to read group list\n"));
		return -EACCES;
	}
	bzero(&st, sizeof(st));
	ret = lstat(path, &st);
	if (ret == -1) {
		DEBUG(DEBUG_ERR, (__location__ " Failed to lstat(%s) %s\n", path, strerror(errno)));
		return -EIO;
	}
	perms = check_permissions(&st, uid, gid, &gl);
	if (!(perms & required)) {
		return -EACCES;
	}

	return 0;
}

int get_owner(char *path, uid_t *uid)
{
	int ret;
	struct stat st;

	bzero(&st, sizeof(st));
	ret = lstat(path, &st);
	if (ret == -1) {
		DEBUG(DEBUG_ERR, (__location__ " Failed to lstat(%s) %s\n", path, strerror(errno)));
		return -EIO;
	}

	*uid = st.st_uid;
	return 0;
}

static void unlink_object(const char *path)
{
	char *rmcmd = NULL;
	int ret;
	struct stat st;

	bzero(&st, sizeof(st));
	ret = lstat(path, &st);
	if ((ret == -1) && (errno == ENOENT)) {
		errno = 0;
		return;
	}

	if ( (st.st_mode & S_IFMT) == S_IFREG) {
		unlink(path);
		return;
	}

	if (asprintf(&rmcmd, "rm -rf %s", path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc rmcmd\n"));
		goto finished;
	}
	ret = system(rmcmd);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,("%s failed with ret:%d\n", rmcmd, ret));
	}

finished:
	if (rmcmd != NULL) {
		free(rmcmd);
	}
}



#define MAX_NAME_LEN 255
struct cached_dir_entry {
	char filename[MAX_NAME_LEN+1];
	struct stat st;
};

char *get_parent_path(const char *path)
{
	char *sptr;
	char *ppath = NULL;

	ppath = strdup(path);
	if (ppath == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " failed to strdup ppath\n"));
		goto finished;
	}
	
	sptr = rindex(ppath, '/');
	if (sptr == NULL) {
		free(ppath);
		ppath = NULL;
		goto finished;
	}
	*sptr = 0;

finished:
	return ppath;
}


#define CACHE_DIR_NAME ".   DIRCACHE"

int open_dir_cache_ro(const char *path, struct stat *old_st)
{
	int ret;
	int cache_fd = -1;
	char *cache_path = NULL;
	struct stat cache_st;

	if (asprintf(&cache_path, "%s/%s/%s", cache, path, CACHE_DIR_NAME) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc cache_path\n"));
		cache_fd = -1;
		goto finished;
	}

	/* get size and timestamps of the cache file if it exists */
	bzero(&cache_st, sizeof(cache_st));
	ret = lstat(cache_path, &cache_st);
	if (ret == -1) {
		DEBUG(DEBUG_ERR, (__location__ " Failed lstat(%s) %s(%u)\n", cache_path, strerror(errno), errno));
		cache_fd = -1;
		goto finished;
	}
	if (cache_st.st_mtime != old_st->st_mtime) {
		/* dont use the cache, its too old */
		cache_fd = -1;
		goto finished;
	}

	cache_fd = open(cache_path, O_RDONLY);

finished:
	if (cache_path != NULL) {
		free(cache_path);
	}
	return cache_fd;	
}


int open_parent_cache_ro(const char *path)
{
	char *parent_path = NULL;
	char *ppath = NULL;
	struct stat parent_st;
	int cache_fd = -1;
	int ret;

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		cache_fd = -1;
		goto finished;
	}

	if (asprintf(&parent_path, "%s/%s", remote, ppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc parent_path\n"));
		cache_fd = -1;
		goto finished;
	}
	ret = lstat(parent_path, &parent_st);
	if (ret == -1) {
		errno = 0;
		cache_fd = -1;
		goto finished;
	}

	cache_fd = open_dir_cache_ro(ppath, &parent_st);

finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (parent_path != NULL) {
		free(parent_path);
	}
	return cache_fd;
}

int unlink_parent_dir_cache(const char *path)
{
	char *ppath = NULL;
	char *cache_path = NULL;
	int ret;

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -1;
		goto finished;
	}

	if (asprintf(&cache_path, "%s/%s", ppath, CACHE_DIR_NAME) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc cache_path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	ret = unlink(cache_path);
	DEBUG(DEBUG_DEBUG,("unlink_parent_dir_cache(%s) == %d\n", cache_path, ret));

finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (cache_path != NULL) {
		free(cache_path);
	}
	return ret;
}


int lookup_attribute_in_parent(const char *path, struct stat *st)
{
	char *ppath = NULL;
	char *sptr;
	int cache_fd = -1;
	struct cached_dir_entry cache_dirent;
	int ret = 0;

	ppath = strdup(path);
	if (ppath == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " failed to strdup ppath\n"));
		ret = -1;
		goto finished;
	}

	sptr = rindex(ppath, '/');
	if (sptr == NULL) {
		ret = -1;
		goto finished;
	}

	sptr++;
	if (*sptr == 0) {
		ret = -1;
		goto finished;
	}
	
	cache_fd = open_parent_cache_ro(path);
	if (cache_fd == -1) {
		ret = -1;
		goto finished;
	}

	while(read(cache_fd, &cache_dirent, sizeof(cache_dirent)) == sizeof(cache_dirent)) {
		if (!strcmp(cache_dirent.filename, sptr)) {
			memcpy(st, &cache_dirent.st, sizeof(cache_dirent.st));
			goto finished;
		}
	}
	ret = -1;

finished:
	if (cache_fd) {
		close(cache_fd);
	}
	if (ppath != NULL) {
		free(ppath);
	}
	errno = 0;
	return ret;
}


/* GETATTR, always return the data for the old object on the original share
   let the linux NFS clients cache the metadata using normal attribute caching
   unless file_check_parent_mtime is set in which case we just verify
   the parents mtime and then return the cached attributes for the object.
*/
static int remote_cache_getattr(const char *path, struct stat *st)
{
	char *old_path = NULL;
	char *new_path = NULL;
	char *cache_path = NULL;
	struct stat old_st;
	struct stat cache_st;
	int ret = 0;

	if (file_check_parent_mtime) {
		/* try to get the attributes from the parents cache
		   (if the parents cache is "current"
		*/
		ret = lookup_attribute_in_parent(path, &old_st);
		if (ret != -1) {
			DEBUG(DEBUG_DEBUG, (__location__ " GETATTR:[%s] from parent cache\n", path));
			ret = 0;
			goto finished;
		}			
	}

	
	/*
	 * read the attributes from the remote site
	 */
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	bzero(&old_st, sizeof(old_st));
	ret = lstat(old_path, &old_st);
	if (ret == -1) {
		ret = -errno;
		if (errno == ENOENT) {
			DEBUG(DEBUG_INFO,(__location__ " File '%s' is removed from the remote site. Remove local cache copy (if any)\n", old_path));
			unlink_object(new_path);
		}
	}

	DEBUG(DEBUG_DEBUG, (__location__ " GETATTR: from remote site %s (%s:%d:%d) ret:%s(%d)\n", path, old_path, (int)old_st.st_mtime, (int)old_st.st_size, strerror(-ret), ret));

	/*
	 * if it was a file, then we also check the cached copy if it has
	 * expired
	 */
	if ( (old_st.st_mode & S_IFMT) != S_IFREG) {
		goto finished;
	}

	/*
	 * read the attributes from the local cache directory
	 */
	if (asprintf(&cache_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc cache path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	bzero(&cache_st, sizeof(cache_st));
	if (lstat(cache_path, &cache_st) != 0) {
		goto finished;
	}

	/*
	 * check that the locally cached copy is valid
	 */
	if ((cache_st.st_mtime != old_st.st_mtime)
	||  (cache_st.st_size != old_st.st_size)) {
		DEBUG(DEBUG_INFO, (__location__ " locally cached file %s has expired  (time:%u:%u size:%u:%u)\n", cache_path, (unsigned int)cache_st.st_mtime, (unsigned int)old_st.st_mtime, (unsigned int)cache_st.st_size, (unsigned int)old_st.st_size));
		unlink_object(cache_path);		
	}


finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	if (cache_path != NULL) {
		free(cache_path);
	}
	memcpy(st, &old_st, sizeof(old_st));

	return ret;
}

static int remote_cache_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	int ret;
	char *new_path = NULL;
	char *old_path = NULL;
	char *obj_path = NULL;
	char *cache_path = NULL;
	DIR *dir = NULL;
	struct stat old_st;
	struct stat cache_st;
	struct dirent *dirent = NULL;
	int cache_fd = -1;
	struct cached_dir_entry cache_dirent;
	struct flock lock;
	struct statvfs vfs;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&cache_path, "%s/%s/%s", cache, path, CACHE_DIR_NAME) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc cache_path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " READDIR %s\n", path));

try_again:

	/* check the age of the directory cache.
	   purge all cached directories older than max_dir_cache
	*/
	if (max_dir_cache != 0) {
		time_t ltime;

		ltime = time(NULL); 
		ret = lstat(cache_path, &cache_st);
		if (ret != 0) {
			DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to stat cache file %s ret %d\n", cache_path, ret));
			ret = -errno;
			goto read_remote_build_cache;
		}
		if (ltime < cache_st.st_ctime) {
			DEBUG(DEBUG_ERR, (__location__ " dircache %s has ctime (%u) in the future (ltime:%u)\n", cache_path, (int)cache_st.st_ctime, (int)ltime));
			unlink(cache_path);
		}
		if (ltime > (cache_st.st_ctime + max_dir_cache)) {
			DEBUG(DEBUG_ERR, ("Cache has expired for directory %s. Flushing cache\n", new_path));
			unlink(cache_path);
		}			
	}

	/*
	 * try to open the directory cache file and read from it
	 */
	ret = lstat(cache_path, &cache_st);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to stat cache file %s %s\n", cache_path, strerror(errno)));
		goto read_remote_build_cache;
	}
	ret = lstat(old_path, &old_st);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to stat cache file %s %s\n", cache_path, strerror(errno)));
		goto read_remote_build_cache;
	}
	if (cache_st.st_mtime != old_st.st_mtime) {
		/* dont use the cache, its too old */
		DEBUG(DEBUG_ERR, (__location__ " Directory %s has expired from the cache.\n", old_path));
		unlink(cache_path);
		goto read_remote_build_cache;
	}


	ret = verify_permission(uid, gid, old_path, CAN_READ);
	if (ret != 0) {
		ret = -EACCES;
		goto finished;
	}

	cache_fd = open(cache_path, O_RDONLY);
	if ((cache_fd == -1) && (errno == EACCES)) {
		ret = -errno;
		goto finished;
	}
	if (cache_fd != -1) {
		while(read(cache_fd, &cache_dirent, sizeof(cache_dirent)) == sizeof(cache_dirent)) {
			filler(buf, &cache_dirent.filename[0], &cache_dirent.st, 0);
		}
		goto finished;
	}



	/*
	 * as root, read the directory from the remote site and store it
	 * in our cache
	 */
read_remote_build_cache:
	/* if we have run out of space, we cant build a local cache */
	ret = statvfs(cache, &vfs);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " statfs(%s) failed\n", cache));
		ret = 0;
		goto read_remote;
	}
	if (vfs.f_bfree < dir_min_blocks) {
		if( rate_limit_is_ok() ){
			DEBUG(DEBUG_CRIT, ("Cache is full. Only %d blocks left in %s. Can not build local cache of directory %s/%s\n", (int)vfs.f_bfree, cache, export, path));
		}

		goto read_remote;
	}
	if (vfs.f_ffree < dir_min_inodes) {
		if( rate_limit_is_ok() ){
			DEBUG(DEBUG_CRIT, ("Cache is full. Only %d inodes left in %s. Can not build local cache of directory %s/%s\n", (int)vfs.f_ffree, cache, export, path));
		}

		goto read_remote;
	}




	/*
	 * stat the remote directory and create it in the cache
	 */
	ret = lstat(old_path, &old_st);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to stat cache file %s %s\n", cache_path, strerror(errno)));
		ret = -errno;
		goto finished;
	}
	mkdir(new_path, 0);
	if (chown(new_path, old_st.st_uid, old_st.st_gid) == -1) {
		DEBUG(DEBUG_ERR, ("CHOWN for new directory %s failed with %s\n", new_path, strerror(errno)));
		unlink(cache_path);
		goto finished;
	}
	if (chmod(new_path, old_st.st_mode) == -1) {
		DEBUG(DEBUG_ERR, ("CHMOD for new directory %s failed with %s\n", new_path, strerror(errno)));
		unlink(cache_path);
		goto finished;
	}


	/*
	 * create the dir cache file
	 */
	cache_fd = open(cache_path, O_RDWR|O_CREAT);
	if (cache_fd == -1) {
	DEBUG(DEBUG_ERR, (__location__ " Failed to create/open directory cache\n"));
		goto try_again;
	}
	if (fchown(cache_fd, old_st.st_uid, old_st.st_gid) == -1) {
		DEBUG(DEBUG_ERR, ("CHOWN for dir cache %s failed with errno:%u\n", cache_path, errno));
		unlink(cache_path);
		goto finished;
	}
	if (fchmod(cache_fd, old_st.st_mode) == -1) {
		DEBUG(DEBUG_ERR, ("CHMOD for dir cache %s failed with errno:%u\n", cache_path, errno));
		unlink(cache_path);
		goto finished;
	}

	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 1;
	lock.l_pid = getpid();
	if (fcntl(cache_fd, F_SETLK, &lock) != 0) {
	DEBUG(DEBUG_ERR, (__location__ " Failed to get lock on dircache file  %s   try again later\n", new_path));
		sleep(1);
		close(cache_fd);
		goto try_again;
	}

read_remote:
	dir = opendir(old_path);
	if (dir == NULL) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to open old directory %s\n", old_path));
		unlink(cache_path);
		ret = -errno;
		goto finished;
	}

	errno = 0;
	while ((dirent	= readdir(dir)) != NULL) {
		if (obj_path != NULL) {
			free(obj_path);
			obj_path = NULL;
		}

		if (strlen(dirent->d_name) > MAX_NAME_LEN) {
			DEBUG(DEBUG_ERR, (__location__ " Too long name : %s. skipping entry for %s\n",
				dirent->d_name, cache_path));
			continue;
		}
		memcpy(&cache_dirent.filename[0],dirent->d_name, strlen(dirent->d_name)+1);


		if (asprintf(&obj_path, "%s/%s", old_path, dirent->d_name) == -1) {
			DEBUG(DEBUG_ERR,(__location__ " Failed to malloc obj path\n"));
			ret = -ENOMEM;
			goto finished;
		}

		ret = lstat(obj_path, &cache_dirent.st);
		if (ret != 0) {
			DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: failed to stat %s ret %d\n", obj_path, ret));
			unlink(cache_path);
			ret = -errno;
			goto finished;
		}
		
		if (cache_fd != -1) {
			int r;
			r = write(cache_fd, &cache_dirent, sizeof(cache_dirent));
			if (r != sizeof(cache_dirent)) {
				DEBUG(DEBUG_ERR, (__location__ " write of cached dirent failed for %s\n", cache_path));
				close(cache_fd);
				cache_fd = -1;
				unlink(cache_path);
			}
		}

	}
	if (errno != 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_readdir: readdir returned NULL and set errno to :%d for %s\n", errno, cache_path));
		unlink(cache_path);
		ret = -errno;
	}

	/* if all went well, update the timestamps of our cache to match the
	   directory on the remote server
	*/
	if (cache_fd != -1) {
		struct utimbuf times;
		int r;

		close(cache_fd);
		cache_fd = -1;
		times.actime  = old_st.st_atime;
		times.modtime = old_st.st_mtime;
		r = utime(cache_path, &times);
		if (r == -1) {
			DEBUG(DEBUG_ERR, (__location__ " update cache time failed for %s\n", cache_path));
			unlink(cache_path);
		}
	}

	/* and finally, try it again and hopefully read it from the cache
	 * as the real user this time.
	 */
	errno = 0;
	goto try_again;

finished:
	if (cache_fd != -1) {
		close(cache_fd);
	}
	if (dir) {
		closedir(dir);
	}
	if (obj_path != NULL) {
		free(obj_path);
	}
	if (cache_path != NULL) {
		free(cache_path);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_statfs(const char *path, struct statvfs *buf)
{
	char *old_path = NULL;
	int ret;

	DEBUG(DEBUG_DEBUG, (__location__ " STATFS called\n"));

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old_path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	ret = statvfs(old_path, buf);
	if (ret == -1) {
		ret = -errno;
		goto finished;
	}

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	return ret;
}

static int remote_cache_readlink(const char *path, char *buf, size_t len)
{
	char *old_path = NULL;
	ssize_t lsize;
	int ret = 0;

	DEBUG(DEBUG_DEBUG, (__location__ " READLINK %s len:%d\n", path, (int)len));

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old_path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	lsize = readlink(old_path, buf, len);
	if (lsize == -1) {
		ret = -errno;
		goto finished;
	}
	if (lsize < len) {
		buf[lsize] = 0;
	}

finished:
	DEBUG(DEBUG_DEBUG, ("READLINK %s len:%d returns %s(%d)\n", old_path, (int)len, strerror(-ret), ret));

	if (old_path != NULL) {
		free(old_path);
	}
	return ret;
}

static int remote_cache_access(const char *path, int mask)
{
	int ret;
	char *old_path = NULL;
	struct stat old_st;
	gid_t my_gid;
	uid_t my_uid;
	int r_is_ok;
	int w_is_ok;
	int x_is_ok;

	DEBUG(DEBUG_DEBUG, (__location__ " ACCESS %s mask 0x%08x\n", path, mask));

	if (!read_write) {
		if (mask & W_OK) {
			DEBUG(DEBUG_DEBUG, (__location__ " Access failed W was not allowed\n"));
			ret = -EROFS;
			goto finished;
		}
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old_path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	bzero(&old_st, sizeof(old_st));
	ret = lstat(old_path, &old_st);
	if (ret == -1) {
		DEBUG(DEBUG_ERR, (__location__ " Failed lstat(%s) %s(%u)\n", old_path, strerror(errno), errno));
		ret = -errno;
		goto finished;
	}

	my_uid = fuse_get_context()->uid;
	my_gid = fuse_get_context()->gid;

	/* check the r bit */
	r_is_ok = 0;
	if (mask & R_OK) {
		if (old_st.st_mode & S_IROTH) {
			r_is_ok = 1;
		}
		if ((old_st.st_mode & S_IRGRP)
		&&  (old_st.st_gid == my_gid) ) {
			r_is_ok = 1;
		}
		if ((old_st.st_mode & S_IRUSR)
		&&  (old_st.st_uid == my_uid) ) {
			r_is_ok = 1;
		}
		if (my_uid == 0) {
			r_is_ok = 1;
		}
	} else {
		r_is_ok = 1;
	}

	/* check the w bit */
	w_is_ok = 0;
	if (mask & W_OK) {
		if (old_st.st_mode & S_IWOTH) {
			w_is_ok = 1;
		}
		if ((old_st.st_mode & S_IWGRP)
		&&  (old_st.st_gid == my_gid) ) {
			w_is_ok = 1;
		}
		if ((old_st.st_mode & S_IWUSR)
		&&  (old_st.st_uid == my_uid) ) {
			w_is_ok = 1;
		}
		if (my_uid == 0) {
			w_is_ok = 1;
		}
	} else {
		w_is_ok = 1;
	}

	if (w_is_ok == 0) {
		DEBUG(DEBUG_DEBUG, (__location__ " Access failed W was not allowed\n"));
		ret = -EACCES;
		goto finished;
	}

	/* check the x bit */
	x_is_ok = 0;
	if (mask & X_OK) {
		if (old_st.st_mode & S_IXOTH) {
			x_is_ok = 1;
		}
		if ((old_st.st_mode & S_IXGRP)
		&&  (old_st.st_gid == my_gid) ) {
			x_is_ok = 1;
		}
		if ((old_st.st_mode & S_IXUSR)
		&&  (old_st.st_uid == my_uid) ) {
			x_is_ok = 1;
		}
		if (my_uid == 0) {
			x_is_ok = 1;
		}
	} else {
		x_is_ok = 1;
	}

	if (x_is_ok == 0) {
		DEBUG(DEBUG_DEBUG, (__location__ " Access failed X was not allowed\n"));
		ret = -EACCES;
		goto finished;
	}

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	return ret;
}


int create_parent_directories(char *old_path, char *new_path)
{
	char *old_dir = NULL;
	char *new_dir = NULL;
	char *p;
	struct stat st;
	int ret, len;
	
	old_dir = strdup(old_path);
	if (old_dir == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " failed to allocate old_dir\n"));
		ret = -1;
		goto finished;
	}
	p = rindex(old_dir, '/');
	if (p == NULL) {
		ret = -1;
		goto finished;
	}
	*p = 0;
	len = strlen(old_dir);
	while(1) {
		len--;
		if (len == 0) {
			break;
		}
		if (old_dir[len] == '/') {
			old_dir[len] = 0;
		} else {
			break;
		}
	}

	new_dir = strdup(new_path);
	if (new_dir == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " failed to allocate new_dir\n"));
		ret = -1;
		goto finished;
	}
	p = rindex(new_dir, '/');
	*p = 0;
	len = strlen(new_dir);
	while(1) {
		len--;
		if (len == 0) {
			break;
		}
		if (new_dir[len] == '/') {
			new_dir[len] = 0;
		} else {

			break;
		}
	}

	DEBUG(DEBUG_DEBUG,("Create parent directories OLD %s : %s  NEW %s : %s\n", old_path, old_dir, new_path, new_dir));

	bzero(&st, sizeof(st));
	ret = lstat(new_dir, &st);
	if ((ret == -1) && (errno == ENOENT)) {
		if (create_parent_directories(old_dir, new_dir) != 0) {
			ret = -1;
			goto finished;
		}

		ret = lstat(old_dir, &st);
		if (ret == -1) {
			DEBUG(DEBUG_ERR,(__location__ " Failed to stat remote directory %s\n", old_dir));
			ret = -1;
			goto finished;
		}

		DEBUG(DEBUG_DEBUG,("Must create parent directory %s\n", new_dir));
		mkdir(new_dir, 0);
		if (chown(new_dir, st.st_uid, st.st_gid) == -1) {
			DEBUG(DEBUG_ERR, ("CHOWN for new directory %s failed with %s\n", new_dir, strerror(errno)));
			ret = -1;
			goto finished;
		}
		if (chmod(new_dir, st.st_mode) == -1) {
			DEBUG(DEBUG_ERR, ("CHMOD for new directory %s failed with %s\n", new_dir, strerror(errno)));
			unlink(new_dir);
			ret = -1;
			goto finished;
		}

	}

	ret = 0;

finished:
	if (old_dir != NULL) {
		free(old_dir);
	}
	if (new_dir != NULL) {
		free(new_dir);
	}
	return ret;
}

int open_migrate_db(void)
{
	int ret = 0;

	/* Open the migrate db database */
	if (migrate_db == NULL) {
		migrate_db_path = get_migrate_db_path(export);
		DEBUG(DEBUG_INFO,("Initializing migration database %s\n", migrate_db_path));
		migrate_db = tdb_open(migrate_db_path, 10000, TDB_CLEAR_IF_FIRST|TDB_NOSYNC, O_RDWR|O_CREAT, 0600);
		if (migrate_db == NULL) {
			DEBUG(DEBUG_ERR,("Failed to initialize migration database.\n"));
			ret = -1;
			goto finished;
		}
	}
finished:
	return ret;
}

/*
 * function to compute a cheap hash of a string.
 * taken from tdb
 */
static unsigned int file_hash(const char *file)
{
	uint32_t value; /* Used to compute the hash value.  */
	uint32_t   i;   /* Used to cycle through random values. */
	int len = strlen(file);

	/* Set the initial value from the key size. */
	for (value = 0x238F13AF * len, i=0; i < len; i++)
		value = (value + (file[i] << (i*5 % 24)));

	return (1103515243 * value + 12345);
}


int mark_file_for_migration(const char *path)
{
	TDB_DATA hdrkey, pathkey, data;
	struct migrate_header *mh = NULL;
	struct migrate_entry *me = NULL;
	int ret = 0;
	unsigned int hash;
#define HASH_LIST_SIZE 16
	static unsigned int hashes[HASH_LIST_SIZE];
	static int hash_idx = 0;
	int i;

	hash = file_hash(path);
	for(i=0; i<HASH_LIST_SIZE; i++) {
		if (hash == hashes[i]) {
			return 0;
		}
	}
	hash_idx = (hash_idx+1)%HASH_LIST_SIZE;
	hashes[hash_idx] = hash;

	pthread_mutex_lock(&migrate_db_mutex);

	DEBUG(DEBUG_DEBUG,("Marking file %s for migration\n", path));
	open_migrate_db();

	/* get the header */
	hdrkey.dptr  = (uint8_t *)discard_const(MIGRATE_HEADER_KEY);
	hdrkey.dsize = strlen(MIGRATE_HEADER_KEY);

	ret = tdb_lockall(migrate_db);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to lock migration database\n"));
		ret = -1;
		goto finished;
	}

	data = tdb_fetch(migrate_db, hdrkey);
	if (data.dptr == NULL) {
		mh = malloc(sizeof(struct migrate_header));
		if (mh == NULL) {
			DEBUG(DEBUG_ERR,(__location__ " Failed to allocate new mh structure\n"));
			ret = -ENOMEM;
			goto finished;
		}
		mh->first = 0;
		mh->last  = 0;
		mh->max   = MIGRATE_MAX;
	} else {
		mh = malloc(data.dsize);
		if (mh == NULL) {
			free(data.dptr);
			DEBUG(DEBUG_ERR,(__location__ " Failed to allocate copy mh structure\n"));
			ret = -ENOMEM;
			goto finished;
		}
		memcpy(mh, data.dptr, data.dsize);
		free(data.dptr);
	}

	mh->last = (mh->last + 1)%mh->max; 
	if (mh->last == mh->first) {
		/* we have wrapped, discard the oldest entry */
		mh->first = (mh->first + 1)%mh->max; 
	}

	/* store the new path. we use the chainlock on the header record
	   as our lock to the database */
	data.dsize = offsetof(struct migrate_entry, path) + strlen(path);
	me  = malloc(data.dsize);
	if (me == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to allocate path data\n"));
		ret = -ENOMEM;
		goto finished;
	}
	data.dptr = (uint8_t *)me;

	me->ts = time(NULL);
	me->pathlen = strlen(path);
	memcpy(&me->path[0], path, me->pathlen);

	DEBUG(DEBUG_DEBUG,("Marking file %s for migration ts:%d first:%d last:%d\n", path, (int)me->ts, mh->first, mh->last));

	pathkey.dptr  = (uint8_t *)&mh->last;
	pathkey.dsize = sizeof(mh->last);
	ret = tdb_store(migrate_db, pathkey, data, TDB_REPLACE);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to store path for migration\n"));
		goto finished;
	}

	
	data.dptr  = (uint8_t *)mh;
	data.dsize = sizeof(*mh);
	ret = tdb_store(migrate_db, hdrkey, data, TDB_REPLACE);
	if (ret != 0 ){
		DEBUG(DEBUG_ERR,(__location__ " Failed to store migration header\n"));
		goto finished;
	}

	ret = kill(migpid, SIGUSR1);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,("Failed to send signal to migration daemon\n"));
		goto finished;
	}

finished:
	tdb_unlockall(migrate_db);
	pthread_mutex_unlock(&migrate_db_mutex);
	if (mh != NULL) {
		free(mh);
	}
	if (me != NULL) {
		free(me);
	}
	return ret;
}

static int migrate_small_file_immed(char *old_path, char *new_path, struct stat *st)
{
	char *new_tmppath = NULL;
	char *buf = NULL;
	int ret = 0;
	int ofd = -1;
	int nfd = -1;
	struct utimbuf times;
	struct flock lock;
	int has_lock = 0;

	DEBUG(DEBUG_DEBUG,("MIGRATE_SMALL_FILE_IMMED(%s %s)\n", old_path, new_path));
	if (asprintf(&new_tmppath, "%s ... TMP_RC", new_path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new_tmppath\n"));
		ret = -ENOMEM;
		goto finished;
	}

	if (st->st_size == 0) {
		ret = -ENOMEM;
		goto finished;
	}

	nfd = open(new_tmppath, O_RDWR|O_CREAT, st->st_mode);
	if (nfd == -1) {
		ret = -errno;
		goto finished;
	}

	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 1;
	lock.l_pid = getpid();
	if (fcntl(nfd, F_SETLK, &lock) != 0) {
		DEBUG(DEBUG_INFO, (__location__ " Failed to set lock on file for immediate migration %s\n", new_tmppath));
		ret = -errno;
		goto finished;
	}
	has_lock = 1;

	ofd = open(old_path, O_RDONLY);
	if (ofd == -1) {
		DEBUG(DEBUG_ERR, (__location__ " READ: failed to open remote file %s %s\n", old_path, strerror(errno)));
		ret = -errno;
		goto finished;
	}

	buf = malloc(st->st_size);
	if (buf == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc %d bytes\n", (int)st->st_size));
		ret = -ENOMEM;
		goto finished;
	}

	ret = pread(ofd, buf, st->st_size, 0);
	if (ret < 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote pread returned %d\n", ret));
		ret = -errno;
		goto finished;
	}
	if (ret != st->st_size) {
		DEBUG(DEBUG_ERR, (__location__ " remote pread returned %d while expecting %d\n", ret, (int)st->st_size));
		ret = -EIO;
		goto finished;
	}

	ret = pwrite(nfd, buf, st->st_size, 0);
	if (ret < 0) {
		DEBUG(DEBUG_ERR, (__location__ " local pwrite returned %d\n", ret));
		ret = -errno;
		goto finished;
	}
	if (ret != st->st_size) {
		DEBUG(DEBUG_ERR, (__location__ " local pwrite returned %d expected %d\n", ret, (int)st->st_size));
		ret = -EIO;
		goto finished;
	}
	ret = 0;

	if (chown(new_tmppath, st->st_uid, st->st_gid) == -1) {
		DEBUG(DEBUG_ERR, (__location__ " chown for migrated file %s failed with %s\n", new_tmppath, strerror(errno)));
		ret = -errno;
		goto finished;
	}

	if (chmod(new_tmppath, st->st_mode) == -1) {
		DEBUG(DEBUG_ERR, (__location__ " chmod for migrated file %s failed with %s\n", new_tmppath, strerror(errno)));
		ret = -errno;
		goto finished;
	}
	times.actime  = st->st_atime;
	times.modtime = st->st_mtime;
	if (utime(new_tmppath, &times) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " failed to update time for %s\n", new_tmppath));
		ret = -errno;
		goto finished;
	}

	unlink(new_path);

	if (rename(new_tmppath, new_path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " failed to rename %s to %s %s\n", new_tmppath, new_path, strerror(errno)));
		ret = -errno;
		goto finished;
	}

finished:
	if (ofd != -1) {
		close(ofd);
	}
	if (nfd != -1) {
		close(nfd);
	}
	if ((has_lock != 0) && (ret != 0)) {
		unlink(new_tmppath);
	}
	if (new_tmppath != NULL) {
		free(new_tmppath);
	}
	if (buf != NULL) {
		free(buf);
	}
	return ret;
}


static int remote_cache_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	char *new_path = NULL;
	char *old_path = NULL;
	struct stat cache_st;
	struct stat old_st;
	struct stat new_st;
	int has_old_st = 0;
	int has_new_st = 0;
	int ret = 0;
	int fd = -1;
	struct statvfs vfs;
	time_t ltime;
	int perms;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	struct group_list gl;

	DEBUG(DEBUG_DEBUG, (__location__ " READ: %s offset:%d length:%d\n", path, (int)offset, (int)size));

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	ret = get_group_list(uid, &gl);
	if (ret != 0) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to read group list\n"));
		ret = -EACCES;
		goto finished;
	}


	/*
	 * If we should check the mtime of the file itself
	 * instead of using the trick to check the mtime of the parent
	 * directory instead. Note, that trick only works if files are
	 * created and deleted but never modified.
	 */
	if (file_check_parent_mtime == 0) {
		/* read attributes from local cache */
		bzero(&new_st, sizeof(new_st));
		ret = lstat(new_path, &new_st);
		if (ret == -1) {
			DEBUG(DEBUG_DEBUG, (__location__ " Failed to lstat(%s) %s\n", new_path, strerror(errno)));
			goto read_from_remote_site;
		}
		has_new_st=1;

		/* read attributes from remote site */
		bzero(&old_st, sizeof(old_st));
		ret = lstat(old_path, &old_st);
		if (ret == -1) {
			DEBUG(DEBUG_ERR, (__location__ " Failed to lstat(%s) %s\n", old_path, strerror(errno)));
			goto finished;
		}
		has_old_st=1;

		if ((new_st.st_mtime != old_st.st_mtime)
		||  (new_st.st_size != old_st.st_size)) {
			DEBUG(DEBUG_INFO, (__location__ " locally cached file %s has expired  (time:%u:%u size:%u:%u)\n", new_path, (unsigned int)new_st.st_mtime, (unsigned int)old_st.st_mtime, (unsigned int)new_st.st_size, (unsigned int)old_st.st_size));
			unlink_object(new_path);		
			goto read_from_remote_site;
		}

		goto read_from_local_cache;
	}

	/* we use the trick to check the mtime of the parent directory
	 * instead of the file itself. This reduses the pressure on the
	 * attribute cache and speeds things up quite a lot.
	 * This can only be used when files are created/deleted but never
	 * modified.
	 */
	ret = lookup_attribute_in_parent(path, &cache_st);
	if (ret == -1) {
		DEBUG(DEBUG_ERR, (__location__ " READ: %s not found in parent cache\n", path));
		goto read_from_remote_site;
	}			

	if (has_new_st == 0) {
		bzero(&new_st, sizeof(new_st));
		ret = lstat(new_path, &new_st);
		if (ret == -1) {
			DEBUG(DEBUG_ERR, (__location__ " Failed to lstat(%s) %s\n", new_path, strerror(errno)));
			goto read_from_remote_site;
		}
	}
	if ((new_st.st_mtime != cache_st.st_mtime)
	||  (new_st.st_size != cache_st.st_size)) {
		DEBUG(DEBUG_ERR, (__location__ " can not read from local cached file\n"));
		goto read_from_remote_site;
	}

read_from_local_cache:
	/*
	 * read from local cache
	 */
	errno = 0;
	DEBUG(DEBUG_DEBUG, (__location__ " READ: %s from local cache\n", path));
	perms = check_permissions(&old_st, uid, gid, &gl);
	if (!(perms & CAN_READ)) {
		ret = -EACCES;
		goto finished;
	}

	fd = open(new_path, O_RDONLY);
	if (fd == -1) {
		if (errno != EACCES) {
			DEBUG(DEBUG_ERR, (__location__ " READ: failed to open %s %s(%u)\n", new_path, strerror(errno), errno));
		}
		ret = -errno;
		goto finished;
	}
	ret = pread(fd, buf, size, offset);
	if (ret < 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_read: local pread returned %d\n", ret));
		ret = -errno;
		goto finished;
	}
	goto finished;


	/* 
	 * read from remote site
	 * start a background job to migrate the entire file
	 */
read_from_remote_site:
	errno = 0;
	DEBUG(DEBUG_DEBUG, (__location__ " READ: %s from remote site\n", path));

	if (has_old_st == 0) {
		if (lstat(old_path, &old_st) == -1) {
			DEBUG(DEBUG_ERR, (__location__ " Failed to stat remote file %s when trying to migrate\n", old_path));
			goto finished;
		}
		has_old_st = 1;
	}

	perms = check_permissions(&old_st, uid, gid, &gl);
	if (!(perms & CAN_READ)) {
		ret = -EACCES;
		goto finished;
	}

	/* if the file is smaller than this we just copy the entire file
	 * in one go
	 */
	if (old_st.st_size <= file_migrate_small_file) {
		ret = migrate_small_file_immed(old_path, new_path, &old_st);
		if (ret == 0) {
				DEBUG(DEBUG_DEBUG,("migrate_small_file_immed() successful reread from cache\n"));
			goto read_from_local_cache;
		}
	}


	fd = open(old_path, O_RDONLY);
	if (fd == -1) {
		DEBUG(DEBUG_ERR, (__location__ " READ: failed to open remote file %s %s\n", old_path, strerror(errno)));
		ret = -errno;
		goto finished;
	}
	/* should forcibly invalidate parents cache here ? */

	ret = pread(fd, buf, size, offset);
	if (ret < 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_read: remote pread returned %d\n", ret));
		ret = -errno;
		goto finished;
	}



	if ( (file_max_size > 0)
	&&   (old_st.st_size>>20) > file_max_size) {
		if( rate_limit_is_ok() ){
			DEBUG(DEBUG_ERR,("file %s is too big for the cache %u MB\n", old_path, (int)(old_st.st_size>>20)));
		}
		goto finished;
	}

	/* if we have run out of space, we cant build a local cache */
	if (statvfs(cache, &vfs) != 0) {
		DEBUG(DEBUG_ERR, (__location__ " statfs(%s) failed\n", cache));
		goto finished;
	}
	if (vfs.f_bfree < file_min_blocks) {
		if( rate_limit_is_ok() ){
			DEBUG(DEBUG_CRIT, ("Cache is full. Only %d blocks left in %s. Can not add file %s to the cache\n", (int)vfs.f_bfree, cache, old_path));
		}

		goto finished;
	}
	if (vfs.f_ffree < file_min_inodes) {
		if( rate_limit_is_ok() ){
			DEBUG(DEBUG_CRIT, ("Cache is full. Only %d inodes left in %s. Can not add file %s to the cache\n", (int)vfs.f_ffree, cache, old_path));
		}

		goto finished;
	}

	/* only migrate the file if it was last modified 
	 * file_min_mtime_age seconds ago
	 */
	ltime = time(NULL);
	if (ltime < (old_st.st_mtime + file_min_mtime_age)) {
		DEBUG(DEBUG_INFO, (__location__ " file '%s' has changed too recently (%d seconds ago, limit is %d seconds). Skipping migration\n", old_path, (int)(ltime-old_st.st_mtime), file_min_mtime_age));

		goto finished;
	}

	mark_file_for_migration(path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	if (fd != -1) {
		close(fd);
		fd = -1;
	}
	return ret;
}

static int remote_cache_mknod(const char *path, mode_t mode, dev_t rdev)
{
	char *new_path = NULL;
	char *old_path = NULL;
	uid_t uid = fuse_get_context()->uid;
	int ret;

	if (!read_write) {	
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " MKNOD:%s mode:%o rdev:%x\n", path, (int)mode, (int)rdev));

	if (uid != 0) {
		ret = -EPERM;
		goto finished;
	}

	ret = mknod(old_path, mode, rdev);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " MKNOD %s mode:%o rdev:%x failed with errno:%u\n", old_path, mode, (int)rdev, errno));
		ret = -errno;
		goto finished;
	}

	/* if the mknod was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}



static int remote_cache_mkdir(const char *path, mode_t mode)
{
	char *new_path = NULL;
	char *old_path = NULL;
	char *old_ppath = NULL;
	char *ppath = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_ppath, "%s/%s", remote, ppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old ppath\n"));
		ret = -ENOMEM;
		goto finished;
	}



	DEBUG(DEBUG_DEBUG, (__location__ " MKDIR:%s mode:%o\n", path, (int)mode));

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	ret = mkdir(old_path, mode);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " MKDIR %s mode:%o failed with errno:%u\n", old_path, mode, errno));
		ret = -errno;
		goto finished;
	}
	ret = chown(old_path, uid, gid);
	if (ret == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to chown() %s\n", old_path));
		//qqq if the chmod failed we must delete the directory
		ret = -errno;
		goto finished;
	}

	/* if the mkdir was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}


static int remote_cache_rmdir(const char *path)
{
	char *new_path = NULL;
	char *old_path = NULL;
	char *old_ppath = NULL;
	char *ppath = NULL;
	int ret;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

       	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " RMDIR:%s\n", path));

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	ret = rmdir(old_path);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " RMDIR %s failed with errno:%u\n", old_path, errno));
		ret = -errno;
		goto finished;
	}

	/* if the rmdir was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}



static int remote_cache_unlink(const char *path)
{
	char *new_path = NULL;
	char *old_path = NULL;
	char *old_ppath = NULL;
	char *ppath = NULL;
	int ret;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_ppath, "%s/%s", remote, ppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old ppath\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " UNLINK:%s\n", path));

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	/* unlink the file on the remote site */
	ret = unlink(old_path);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " UNLINK %s failed with errno:%u\n", old_path, errno));
		ret = -errno;
		goto finished;
	}

	/* if the unlink was successful we try to delete any local cached
	   entries for this name and remove the cache for this directory
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);
finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_chmod(const char *path, mode_t mode)
{
	char *new_path = NULL;
	char *old_path = NULL;
	uid_t obj_uid;
	uid_t uid = fuse_get_context()->uid;
	int ret;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	DEBUG(DEBUG_DEBUG, (__location__ " CHMOD:%s mode:%o\n", path, mode));

	ret = get_owner(old_path, &obj_uid);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " get_owner failed\n"));
		goto finished;
	}

	if ((uid != obj_uid) && (uid != 0)) {
		ret = -EPERM;
		goto finished;
	}

	ret = chmod(old_path, mode);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " CHMOD %s mode %o failed with errno:%u\n", old_path, mode, errno));
		ret = -errno;
		goto finished;
	}

	/* if the chmod was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_chown(const char *path, uid_t uid, gid_t gid)
{
	char *old_path = NULL;
	uid_t myuid = fuse_get_context()->uid;
	int ret;

	DEBUG(DEBUG_DEBUG, (__location__ " CHOWN:%s uid %d gid %d\n", path, uid, gid));

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (myuid != 0) {
		ret = -EPERM;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	ret = chown(old_path, uid, gid);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " CHOWN %s uid %d gid %d failed with errno:%u\n", old_path, uid, gid, errno));
		ret = -errno;
		goto finished;
	}

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	return ret;
}

static int remote_cache_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
	char *new_path = NULL;
	char *old_path = NULL;
	char *old_ppath = NULL;
	char *ppath = NULL;
	int ret;
	int fd;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}
	
	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_ppath, "%s/%s", remote, ppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old ppath\n"));
		ret = -ENOMEM;
		goto finished;
	}

	DEBUG(DEBUG_DEBUG, (__location__ " CREATE %s mode %o\n", path, mode));

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	fd = creat(old_path, mode);
	if (fd == -1) {
		DEBUG(DEBUG_DEBUG,(__location__ " CREATE %s mode %o failed with errno:%u\n", old_path, mode, errno));
		ret = -errno;
		goto finished;
	}
	close(fd);

	ret = chown(old_path, uid, gid);
	if (ret == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to chown() %s\n", old_path));
		//qqq delete the file if the chown failed  check other uses
		ret = -errno;
		goto finished;
	}

	/* if the creat was successful we try to delete any local cached
	   entries for this name
	*/
	ret = 0;
#if 0
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);
#endif

finished:
	if (ppath != NULL) {
		free(ppath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_utime(const char *path, struct utimbuf *times)
{
	char *new_path = NULL;
	char *old_path = NULL;
	uid_t obj_uid;
	uid_t uid = fuse_get_context()->uid;
	int ret;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " UTIME %s\n", path));

	ret = get_owner(old_path, &obj_uid);
	if (ret != 0) {
		DEBUG(DEBUG_ERR, (__location__ " get_owner failed\n"));
		goto finished;
	}

	if ((uid != obj_uid) && (uid != 0)) {
		ret = -EPERM;
		goto finished;
	}

	ret = utime(old_path, times);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " UTIME %s failed with errno:%u\n", old_path, errno));
		ret = -errno;
		goto finished;
	}

	/* if the utime was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_write_internal(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *ffi)
{
	char *new_path = NULL;
	char *old_path = NULL;
	int fd = -1;
	int ret;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	ret = verify_permission(uid, gid, old_path, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	fd = open(old_path, O_RDWR);
	if (fd == -1) {
		if (errno != EACCES) {
			DEBUG(DEBUG_ERR, (__location__ " WRITE: failed to open %s %s(%u)\n", new_path, strerror(errno), errno));
		}
		ret = -errno;
		goto finished;
	}
	ret = pwrite(fd, buf, size, offset);
	if (ret < 0) {
		DEBUG(DEBUG_ERR, (__location__ " remote_cache_write: local pwrite returned %d\n", ret));
		ret = -errno;
		goto finished;
	}


#if 0
	/* We dont delete the cache at this point since these operations are
	   very expensive. Instead we rely on it being caught when checking the
	   mtime on next read.
	*/
	/* if the pwrite was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);
#endif

finished:
	if (fd != -1) {
		close(fd);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *ffi)
{
	struct fh_struct *fhs = (struct fh_struct *)ffi->fh;

	if (!read_write) {
		return -EROFS;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " WRITE %s offset %d len %d\n", path, (int)offset, (int)size));
	/* writes suck for nfsd for kernels < 2.6.27 since it is all
	   split up into a single threaded sequence of many many tiny writes
	   (average 2k each).
	   Therefore we trigger on when the pid is nfsd and if so we only
	   perform the first write of an i/o (so we can return -ERR proper).
	   All other writes are added to a write list which we concatenate
	   to a single buffer and write again in the _RELEASE() method.

	   this is ugly.
	*/
	if (fhs == NULL) {
		DEBUG(DEBUG_ERR,(__location__ " ERROR: FHS is NULL for %s o:%d s:%d\n", path, (int)offset, (int)size));
		goto finished;
	}

	if(fhs->is_nfsd) {
		struct write_fragment *wf;

		fhs->num_writes++;

		wf = malloc(sizeof(struct write_fragment));
		if (wf == NULL) {
			DEBUG(DEBUG_ERR,("Failed to allocate write fragment structure\n"));
			return -ENOMEM;
		}
		wf->offset = offset;
		wf->size   = size;
		wf->data   = malloc(size);
		if (wf->data == NULL) {
			DEBUG(DEBUG_ERR,("Failed to allocate wf data segment  size:%d\n", (int)size));
			free(wf);
			return -ENOMEM;
		}
		memcpy(wf->data, buf, size);
		wf->next   = fhs->fragments;
		fhs->fragments = wf;

		/* This was not the first write  so just return and defer
		   the write of this data until the _RELEASE() method.
		*/
		if (fhs->num_writes > 1) {
			return size;
		}
	}

finished:
	return remote_cache_write_internal(path, buf, size, offset, ffi);
}

static int remote_cache_truncate(const char *path, off_t offset)
{
	char *new_path = NULL;
	char *old_path = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}


	DEBUG(DEBUG_DEBUG, (__location__ " TRUNCATE %s offset %d\n", path, (int)offset));

	ret = verify_permission(uid, gid, old_path, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	ret = truncate(old_path, offset);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " TRUNCATE %s offset %d failed with errno:%u\n", old_path, (int)offset, errno));
		ret = -errno;
		goto finished;
	}

	/* if the truncate was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
	char *new_path = NULL;
	char *old_path = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&new_path, "%s/%s", cache, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc new path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}

	DEBUG(DEBUG_DEBUG, (__location__ " FTRUNCATE %s offset %d\n", path, (int)offset));

	ret = verify_permission(uid, gid, old_path, CAN_WRITE);
	if (ret != 0) {
		goto finished;
	}

	ret = truncate(old_path, offset);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " FTRUNCATE %s offset %d failed with errno:%u\n", old_path, (int)offset, errno));
		ret = -errno;
		goto finished;
	}

	/* if the ftruncate was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_parent_dir_cache(new_path);

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (new_path != NULL) {
		free(new_path);
	}
	return ret;
}

static int remote_cache_link(const char *path, const char *newpath)
{
	char *old_path = NULL;
	char *old_newpath = NULL;
	char *newppath = NULL;
	char *old_newppath = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	DEBUG(DEBUG_DEBUG, (__location__ " LINK %s -> %s\n", path, newpath));

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	newppath = get_parent_path(newpath);
	if (newppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_newpath, "%s/%s", remote, newpath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old newpath\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_newppath, "%s/%s", remote, newppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old ppath\n"));
		ret = -ENOMEM;
		goto finished;
	}

	ret = verify_permission(uid, gid, old_newppath, CAN_WRITE);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,("Directory does not allow symlink\n"));
		goto finished;
	}

	ret = link(old_path, old_newpath);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " LINK %s -> %s failed with errno:%u\n", old_path, old_newpath, errno));
		ret = -errno;
		goto finished;
	}

finished:
	DEBUG(DEBUG_DEBUG, (__location__ " LINK %s -> %s returned %s(%d)\n", old_path, old_newpath, strerror(-ret), ret));

	if (newppath != NULL) {
		free(newppath);
	}
	if (old_newppath != NULL) {
		free(old_newppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	if (old_newpath != NULL) {
		free(old_newpath);
	}
	return ret;
}


static int remote_cache_symlink(const char *linkname, const char *path)
{
	char *old_path = NULL;
	char *ppath = NULL;
	char *old_ppath = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	DEBUG(DEBUG_DEBUG, (__location__ " SYMLINK %s -> %s\n", path, linkname));

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	ppath = get_parent_path(path);
	if (ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_ppath, "%s/%s", remote, ppath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old ppath\n"));
		ret = -ENOMEM;
		goto finished;
	}

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,("Directory does not allow symlink\n"));
		goto finished;
	}

	ret = symlink(linkname, old_path);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " SYMLINK %s -> %s failed with errno:%s(%u)\n", old_path, linkname, strerror(errno), errno));
		ret = -errno;
		goto finished;
	}

finished:
	DEBUG(DEBUG_DEBUG, (__location__ " SYMLINK %s -> %s returned %s(%d)\n", old_path, linkname, strerror(-ret), ret));

	if (ppath != NULL) {
		free(ppath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_path != NULL) {
		free(old_path);
	}
	return ret;
}

static int remote_cache_rename(const char *path, const char *newpath)
{
	char *old_path = NULL;
	char *old_newpath = NULL;
	char *old_ppath = NULL;
	char *old_newppath = NULL;
	uid_t uid = fuse_get_context()->uid;
	gid_t gid = fuse_get_context()->gid;
	int ret;

	DEBUG(DEBUG_DEBUG, (__location__ " RENAME %s -> %s\n", path, newpath));

	if (!read_write) {
		ret = -EROFS;
		goto finished;
	}

	if (asprintf(&old_path, "%s/%s", remote, path) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old path\n"));
		ret = -ENOMEM;
		goto finished;
	}
	if (asprintf(&old_newpath, "%s/%s", remote, newpath) == -1) {
		DEBUG(DEBUG_ERR,(__location__ " Failed to malloc old newpath\n"));
		ret = -ENOMEM;
		goto finished;
	}
	old_ppath = get_parent_path(old_path);
	if (old_ppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}
	old_newppath = get_parent_path(old_newpath);
	if (old_newppath == NULL) {
		ret = -ENOMEM;
		goto finished;
	}

	ret = verify_permission(uid, gid, old_ppath, CAN_WRITE);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,("SRC directory does not allow rename\n"));
		goto finished;
	}

	ret = verify_permission(uid, gid, old_newppath, CAN_WRITE);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,("DST directory does not allow rename\n"));
		goto finished;
	}

	ret = rename(old_path, old_newpath);
	if (ret != 0) {
		DEBUG(DEBUG_DEBUG,(__location__ " RENAME %s -> %s failed with errno:%u\n", old_path, old_newpath, errno));
		ret = -errno;
		goto finished;
	}

#if 0
	/* if the link was successful we try to delete any local cached
	   entries for this name
	*/
	unlink_object(new_path);
	unlink_object(new_newpath);
	unlink_parent_dir_cache(new_path);
#endif

finished:
	if (old_path != NULL) {
		free(old_path);
	}
	if (old_newpath != NULL) {
		free(old_newpath);
	}
	if (old_ppath != NULL) {
		free(old_ppath);
	}
	if (old_newppath != NULL) {
		free(old_newppath);
	}
	return ret;
}




static int remote_cache_open(const char *path, struct fuse_file_info *ffi)
{
	pid_t pid = fuse_get_context()->pid;
	int fd = -1;
	struct fh_struct *fhs = NULL;
	char *st = NULL;
	char str[256], *pstr;
	
	ffi->fh = 0;

	if (asprintf(&st, "/proc/%d/stat", pid) == -1) {
		DEBUG(DEBUG_ERR,("Failed to create pid-stat string\n"));
		goto finished;
	}	
	if ((fd = open(st, O_RDONLY)) == -1) {
		DEBUG(DEBUG_ERR,("Failed to open pidfile %s   errno:%d\n", st, errno));
		goto finished;
	}	
	str[0] = 0;
	read(fd, str, 256);

	pstr = index(str, '(');
	if (pstr == NULL) {
		DEBUG(DEBUG_ERR,("Failed to find the '('\n"));
		goto finished;
	}	


	fhs = malloc(sizeof(struct fh_struct));
	if (fhs == NULL) {
		DEBUG(DEBUG_ERR,("Failed to allocate fhs structure\n"));
		goto finished;
	}
	bzero(fhs, sizeof(struct fh_struct));
	ffi->fh = (uint64_t)fhs;
DEBUG(DEBUG_ERR,("_open(%s) fhs:%p ffi->fh:%d\n", path, fhs, (int)ffi->fh));
	pstr++;
	if (!strncmp(pstr, "nfsd", 4)) {
		fhs->is_nfsd     = 1;
		fhs->num_writes  = 0;
	}

finished:
	if (fd != -1) {
		close(fd);
	}
	if (st != NULL) {
		free(st);
	}
       	return 0;
}
static int remote_cache_release(const char *path, struct fuse_file_info *ffi)
{
	struct fh_struct *fhs = (struct fh_struct *)ffi->fh;
	char *buf = NULL;

	DEBUG(DEBUG_DEBUG,(__location__ " RELEASE %s\n", path));
DEBUG(DEBUG_ERR,("_release(%s) fhs:%p ffi->fh:%d\n", path, fhs, (int)ffi->fh));
	if (fhs && fhs->is_nfsd && (fhs->num_writes > 1)) {
		size_t size = 0;
		off_t offset = fhs->fragments->offset;
		struct write_fragment *wf, *wfnext;
		
		DEBUG(DEBUG_DEBUG,("RELEASE for NFSD: num_writes:%d\n", fhs->num_writes));
		for(wf=fhs->fragments; wf; wf=wf->next){
			if (wf->offset < offset) {
				offset = wf->offset;
			}
		}
		for(wf=fhs->fragments; wf; wf=wf->next){
			if ((wf->size+wf->offset-offset) > size) {
				size = wf->size+wf->offset-offset;
			}
		}
		buf = malloc(size);
		if (buf == NULL) {
			DEBUG(DEBUG_ERR,("Failed to allocate reassembly buffer of size :%d\n", (int)size));
			goto finished;
		}
		for(wf=fhs->fragments; wf; wf=wf->next){
			memcpy(buf+wf->offset-offset, wf->data, wf->size);
		}
		for(wf=fhs->fragments; wf; wf=wfnext) {
			wfnext = wf->next;
			free(wf->data);
			free(wf);
		}

		DEBUG(DEBUG_DEBUG,("Flushing %d of I/O for NFSD at offset:%d\n", (int)size, (int)offset));
		remote_cache_write_internal(path, buf, size, offset, ffi);
	}

finished:
	if (fhs != NULL) {
		ffi->fh = 0;
		free(fhs);
	}
	if (buf != NULL) {
		free(buf);
	}
	return 0;
}


static struct fuse_operations remote_cache_ops = {
	.getattr	= remote_cache_getattr,
	.readdir	= remote_cache_readdir,
	.statfs		= remote_cache_statfs,
	.readlink	= remote_cache_readlink,
	.access		= remote_cache_access,
	.read		= remote_cache_read,


	.unlink		= remote_cache_unlink,
	.create		= remote_cache_create,
	.mknod		= remote_cache_mknod,
	.mkdir		= remote_cache_mkdir,
	.rmdir		= remote_cache_rmdir,
	.rename		= remote_cache_rename,
	.chmod		= remote_cache_chmod,
	.chown		= remote_cache_chown,
	.utime		= remote_cache_utime,
	.write		= remote_cache_write,
	.truncate	= remote_cache_truncate,
	.ftruncate	= remote_cache_ftruncate,
	.symlink	= remote_cache_symlink,
	.link		= remote_cache_link,



//	.lock		= remote_cache_lock,
	.open		= remote_cache_open,
	.release	= remote_cache_release,



//	.flush		= migrate_flush,
//	.fsync		= migrate_fsync,
//	.setxattr	= migrate_setxattr,
//	.getxattr	= migrate_getxattr,
//	.listxattr	= migrate_listxattr,
//	.removexattr	= migrate_removexattr,
//	.opendir	= migrate_opendir,
//	.releasedir	= migrate_releasedir,
//	.fsyncdir	= migrate_fsyncdir,
//	.fgetattr	= migrate_fgetattr,
//	.utimens	= migrate_utimens,
};


static void usage(void)
{
	fprintf(stderr, "remote-cache [OPTIONS]\n");
	fprintf(stderr, "  --export=directory : the directory to re-export the data through\n");
	fprintf(stderr, "  --cache=directory  : the local directory to cache data in\n");
	fprintf(stderr, "  --remote=directory : the remote directory to cache\n");
	exit(10);
}


int main(int argc, const char *argv[])
{
	int ret;
	int num_args;
	const char *fuse_argv[16] = {
		"remote_cache",
		"<export>",
		"-oallow_other",
		"-odefault_permissions",
		"-ouse_ino",
//"-oreaddir_ino",
//"-onoforget",
		"-omax_write=32768",
		"-okernel_cache",
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	};
	struct poptOption popt_options[] = {
		POPT_AUTOHELP
		{ "export", 'e', POPT_ARG_STRING, &export, 0, "export directory", "directory" },
		{ "unexport", 'u', POPT_ARG_STRING, &unexport, 0, "directory to unexport", "directory" },
		{ "cache", 'c', POPT_ARG_STRING, &cache, 0, "directory to store the cached data", "directory" },
		{ "remote", 'r', POPT_ARG_STRING, &remote, 0, "directory where the remote nfs data is mounted", "directory" },
		{ "max-dir-cache", 0, POPT_ARG_INT, &max_dir_cache, 0, "maximum number of seconds to cache a directory.", "integer" },
		{ "dir-min-inodes", 0, POPT_ARG_INT, &dir_min_inodes, 0, "minimum free inodes to cache directory (default 100)", "integer" },
		{ "dir-min-blocks", 0, POPT_ARG_INT, &dir_min_blocks, 0, "minimum free blocks to cache directory (default 1000)", "integer" },
		{ "file-min-inodes", 0, POPT_ARG_INT, &file_min_inodes, 0, "minimum free inodes to cache more files (default 100)", "integer" },
		{ "file-min-blocks", 0, POPT_ARG_INT, &file_min_blocks, 0, "minimum free blocks to cache more files (default 10000)", "integer" },
		{ "file-max-size", 0, POPT_ARG_INT, &file_max_size, 0, "maximum size in MB of files to cache", "integer" },
		{ "file-min-mtime-age", 0, POPT_ARG_INT, &file_min_mtime_age, 0, "minimum age in seconds since last change for migration", "integer" },
		{ "debug", 'd', POPT_ARG_INT, &debug_level, 0, "debug level", "integer" },
		{ "file-check-parent-mtime", 0, POPT_ARG_NONE, &file_check_parent_mtime, 0, "check mtime of remote file instead of parent directory mtime", NULL },
		{ "read-write", 0, POPT_ARG_NONE, &read_write, 0, "read/write cache", NULL },
		{ "direct-io", 0, POPT_ARG_NONE, &direct_io, 0, "Use direct_io for better write performance (not kNFSd compatible)", NULL },
		{ "big-writes", 0, POPT_ARG_NONE, &big_writes, 0, "Use big_writes for better write performance", NULL },
		POPT_TABLEEND
	};
	poptContext pc;
	int opt;
	const char **extra_argv;
	int extra_argc = 0;
	struct stat st;

#ifdef REMOTE_CACHE_VERS
#define STR(x) #x
#define XSTR(x) STR(x)
	printf("remote-cache version %s\n", XSTR(REMOTE_CACHE_VERS));
#endif

	pc = poptGetContext(argv[0], argc, argv, popt_options, POPT_CONTEXT_KEEP_FIRST);
	while ((opt = poptGetNextOpt(pc)) != -1) {
		switch (opt) {
		default:
			fprintf(stderr, "Invalid option %s: %s\n", 
				poptBadOption(pc, 0), poptStrerror(opt));
			exit(1);
		}
	}

	/* setup the remaining options for the main program to use */
	extra_argv = poptGetArgs(pc);
	if (extra_argv) {
		extra_argv++;
		while (extra_argv[extra_argc]) extra_argc++;
	}

	if (unexport != NULL) {
		remote_cache_unexport(unexport);
		exit(0);
	}

	if (export == NULL) {
		fprintf(stderr, "You must specify --export\n");
		usage();
	}
	bzero(&st, sizeof(st));
	ret = lstat(export, &st);
	if (ret == -1) {
		fprintf(stderr, "Could not stat() --export %s\n", export);
		perror("error: ");
		fprintf(stderr, "Aborting\n");
		exit(10);
	}
	if ((st.st_mode & S_IFMT) != S_IFDIR) {
		fprintf(stderr, "--export %s is not a directory\n", export);
		fprintf(stderr, "Aborting\n");
		exit(10);
	}


	if (cache == NULL) {
		fprintf(stderr, "You must specify --cache\n");
		usage();
	}
	bzero(&st, sizeof(st));
	ret = lstat(cache, &st);
	if (ret == -1) {
		fprintf(stderr, "Could not stat() --cache %s\n", cache);
		perror("error: ");
		fprintf(stderr, "Aborting\n");
		exit(10);
	}
	if ((st.st_mode & S_IFMT) != S_IFDIR) {
		fprintf(stderr, "--cache %s is not a directory\n", cache);
		fprintf(stderr, "Aborting\n");
		exit(10);
	}

	if (remote == NULL) {
		fprintf(stderr, "You must specify --remote\n");
		usage();
	}
	bzero(&st, sizeof(st));
	ret = lstat(remote, &st);
	if (ret == -1) {
		fprintf(stderr, "Could not stat() --remote %s\n", remote);
		perror("error: ");
		fprintf(stderr, "Aborting\n");
		exit(10);
	}
	if ((st.st_mode & S_IFMT) != S_IFDIR) {
		fprintf(stderr, "--remote %s is not a directory\n", remote);
		fprintf(stderr, "Aborting\n");
		exit(10);
	}


	ret = mkdir(RCVARDIR, 0777);
	if ((ret != 0) && (errno != EEXIST)) {
		printf("ERROR: could not create lock file directory %s\n", RCVARDIR);
		perror("foo");
		exit (10);
	}
	errno = 0;
	ret = creat(FILE_MIGRATE_LIMITER, 0777);
	if (ret == -1) {
		printf("ERROR: could not create lock file %s\n", FILE_MIGRATE_LIMITER);
		exit (10);
	}
	close(ret);
	chmod(FILE_MIGRATE_LIMITER, 0777);

	if (dir_min_inodes < 1) {
		printf("ERROR: --dir-min-inodes must be > 0\n");
		exit(10);
	}

	if (dir_min_blocks < 1) {
		printf("ERROR: --dir-min-blocks must be > 0\n");
		exit(10);
	}

	if (file_min_inodes < 1) {
		printf("ERROR: --file-min-inodes must be > 0\n");
		exit(10);
	}

	if (file_min_blocks < 1) {
		printf("ERROR: --file-min-blocks must be > 0\n");
		exit(10);
	}

	fuse_argv[1] = export;

	printf("Remote caching of %s to temp storage %s exporting through %s\n", remote, cache, export);

	log_fd = creat("/var/log/log.remote-cache", 0777);
	if (log_fd == -1) {
		fprintf(stderr, "Could not open logfile. Aborting\n");
		exit(10);
	}

	num_args = 7;
	if (direct_io) {
		fuse_argv[num_args] = "-odirect_io";
	   	num_args++;
		printf("Using DIRECT_IO\n");
	}
	if (big_writes) {
		fuse_argv[num_args] = "-obig_writes";
	   	num_args++;
		printf("Using BIG_WRITES\n");
	}

	ret = fork();
	if (ret == -1) {
		DEBUG(DEBUG_ERR, ("Failed to fork\n"));
		exit(10);
	}
	if (ret != 0) {
		return 0;
	}

	migpid = start_migration_daemon();
	if (migpid == -1) {
		DEBUG(DEBUG_CRIT,("Failed to spawn migration daemon. Aborting\n"));
		exit(10);
	}

	printf("here we go ...\n");
	return fuse_main(num_args, discard_const(fuse_argv), &remote_cache_ops, NULL);
}


